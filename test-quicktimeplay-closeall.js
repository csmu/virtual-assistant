#!/usr/bin/env osascript -l JavaScript

function run(argv) {
  

		var quickTimePlayer = Application('QuickTime Player') ;
		quickTimePlayer.activate() ;
		delay(0.5);
		quickTimePlayerCloseAll(quickTimePlayer,{afterClosingDelay:0.25}) ;
		console.log('finished') ;
}

function quickTimePlayerCloseAll(quickTimePlayer,afterClosingDelay){
	var documents = quickTimePlayer.documents() ;
	while( documents.length > 0 ) {
		var document = documents[0] ;
		document.close() ;
		delay(afterClosingDelay) ;
		var documents = quickTimePlayer.documents() ;
	}
}