#!/usr/bin/env osascript -l JavaScript

function run(argv) {
	
	var systemEvents = Application('System Events') ;
	var textEdit = Application('TextEdit') ;

	textEdit.activate() ;
	delay(1) ;
	var delayAfterStart = 3
	startDictation(systemEvents,delayAfterStart) ;
	
	
}

/* 
 * https://github.com/dtinth/JXA-Cookbook/wiki/System-Events
 * */
function startDictation(systemEvents,delayAfterStart){
	
	var process = Application('System Events').processes.byName('TextEdit') ;
	var editMenuItems = process.menuBars[0].menuBarItems.byName('Edit').menus[0].menuItems() ;
	var dictationMenuItem = null ;
	var started = false ;
	
	for ( var index = 0 ; index < editMenuItems.length; index++) {
	  var editMenuItem = editMenuItems[index] ;
		var title = editMenuItem.title() ;
		var indexOfDictation = title.indexOf('Dictation') ; 
		if ( indexOfDictation > 0 ) {
			var dictationVerb = title.substr(0,indexOfDictation).trim() ;
			console.log(title);
			console.log(dictationVerb) ;
			editMenuItem.click()
			delay(delayAfterStart) ;
			if ( dictationVerb == 'Start' ) {
				started = true ;
			} else {
				// start after stop or cancel
				startDictation(systemEvents,delayAfterStart) ;
			}
		}
	}
	return started ;
	
}