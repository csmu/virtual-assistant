#!/usr/bin/env osascript -l JavaScript

function run(argv) {
  
	var filePath = null ;
	for( var index = 0 ; index < argv.length; index++){
		if ( argv[index] == '--file' ) {
			index++;
			filePath = argv[index] ;
		}
	} 
	
	if ( filePath ){	
		var textEdit = Application('TextEdit') ;
	  var systemEvents = Application('System Events') ;
		var textEditProcess = systemEvents.processes.byName('TextEdit') ;
	
		textEdit.includeStandardAdditions = true ;
		textEditMakePlainText( textEdit, textEditProcess, {afterClickDelay:2, verbose: true } ) ;
		textEditSaveAsText( textEdit, {filePath: filePath, afterSaveDelay:2, verbose: true } ) ;
		
	} else {
		console.log( 'No --file PATH given. Nothing done.')
	}
}

function textEditMakePlainText(textEdit,textEditProcess,afterClickDelay,verbose){
	
	textEdit.activate() ;
	delay(1) ;
	var formatMenuItems = textEditProcess.menuBars[0].menuBarItems.byName('Format').menus[0].menuItems() ;
	//console.log(formatMenuItems.length) ;
	for ( var index = 0; index < formatMenuItems.length; index++){
		//if ( verbose) console.log(index);		
		var formatMenuItem = formatMenuItems[index] ;
		var title = formatMenuItem.title() ;
		var richIndex = title.indexOf('Rich') ;
		var plainIndex = title.indexOf('Plain') ;
		if ( richIndex > -1 || plainIndex > -1) {
			if( verbose) console.log(formatMenuItem.title()) ;	
			if ( plainIndex > -1 ) {
				formatMenuItem.click() ;
				delay(afterClickDelay) ;
			}
		}
	}
}

function textEditSaveAsText(textEdit,options){
	
	var document = textEdit.documents()[textEdit.documents.length-1];
	var text = document.text() + '\n' ;
	if( options.verbose ) console.log(text) ;
	var text = $.NSString.alloc.initWithString(text) ;
	if( options.verbose ) console.log(text) ;

	text.writeToFileAtomicallyEncodingError(options.filePath, true, $.NSUTF8StringEncoding, null);
	delay( options.afterSaveDelay ) ;
	
}

