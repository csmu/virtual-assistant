#!/usr/bin/env node

'use strict';

var fs = require("fs") ;
var ArgumentParser = require('argparse').ArgumentParser;
var FfmpegCommand = require('fluent-ffmpeg');
var path = require('path') ;

// console.dir( path ) ;

var parser = new ArgumentParser({
  version: '0.0.1',
  addHelp:true,
  description: 'Argparse example'
});
parser.addArgument(
  [ '-d', '--directory' ],
	{ required: true }, 
  {
    help: 'the directory to process'
  }
);
parser.addArgument(
  [ '-t', '--to' ],
	{ required: true } ,
  {
    help: 'file format converting to'
  }
);
parser.addArgument(
  [  '--verbose' ],
	{ defaultValue: false } ,
  {
    help: 'show verbose messages'
  }
);

var args = parser.parseArgs();
if ( args.verbose == '0' ) args.verbose = false ;
if ( args.verbose == 'false' ) args.verbose = false ;

if  ( args.verbose ) console.dir( args ) ;

if ( args.directory ) {
	if ( args.verbose ) console.dir( fs ) ;
	if ( fs.lstat( args.directory, function( error, stats ){
		if ( stats.isDirectory() ) {
			
			var files = fs.readdirSync( args.directory ) ;
			var extensionRegularExpressionPattern = /\.([0-9a-z]+$)/i ;
			var fromExtensions = [ 'wav', 'mp4', 'm4a' ]
			var fromArray = [] ;
			var toArray = [] ;
			var processedArray = [] ;
			var newlyProcessedArray = [] ;
			
			for ( var index = 0; index < files.length; index++){
				var file = files[index] ;
				var extension = file.match(extensionRegularExpressionPattern);
				if ( args.verbose ) console.log(file)	
				if ( extension ){
					if ( fromExtensions.indexOf(extension[1]) > -1 ){
						var filename = file.substring(0,extension.index) ;
						if ( extension[1] == args.to ) {
							/* already n the desired format */
							// console.dir( extension ) ;
							// console.log( extension.index ) ;
							toArray.push( file ) ;
							processedArray.push( filename ) ;
						} else {
							if ( processedArray.indexOf( filename ) == -1 ) {
								// console.log( filename ) ;
								var outputFilePath = path.join( args.directory, filename + '.' + args.to ) ;
								var inputFilePath = path.join( args.directory, file ) ;
								
								//console.log(inputFilePath) ;
								//console.log(outputFilePath) ;
								fromArray.push( file ) ;	
								
								try {
									
									stats = fs.statSync( outputFilePath ) ;
									//console.log( outputFilePath + ' exists.' ) ;
									
								} catch (e) {
									
									if ( e.code == 'ENOENT') {
										
										var command = new FfmpegCommand() ;
										command.filename = filename ;
										command.inputFilePath = inputFilePath ;
										command.outputFilePath = outputFilePath ;
										// console.log( command )
										command.input( inputFilePath )
											.on('end', function() {
												console.log( "created " + this.outputFilePath );
												processedArray.push( filename ) ;
												newlyProcessedArray.push( filename ) ;
											})
											.on('error', function(err) {
												console.log('an error happened: ' + err.message);
											}) 
											.save( outputFilePath ) 
										;	
										
									} else {
											console.dir(e) ;
									}
									
								}
								
								
							}
						}
					}
				}
			}	
			// console.dir( toArray ) ;
			// console.dir( fromArray ) ;
			// console.dir( processedArray ) ;
			// console.dir( newlyProcessedArray ) ;
		}
	}) ) ;
}
