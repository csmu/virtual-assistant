#!/usr/bin/env osascript -l JavaScript

function run(argv) {

	var systemEvents = Application('System Events') ;
	systemEvents.start(systemEvents.currentScreenSaver) ;
	delay(3)
	systemEvents.stop(systemEvents.currentScreenSaver) ;
	delay(3)
	systemEvents.stop(systemEvents.currentScreenSaver) ;
	
}