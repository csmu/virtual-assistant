#!/usr/bin/env osascript -l JavaScript
ObjC.import('AppKit') ;
ObjC.import("Cocoa");

/*
 * based on answer in 
 * http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
 * */
Date.prototype.toISOLocal = function() {
   var yyyy = this.getFullYear().toString();
   var MM = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
	 var hh = this.getHours().toString() ;
	 var mm = this.getMinutes().toString() ;
	 var ss = this.getSeconds().toString() ;
   return yyyy 
	 	+ '-' + (MM[1]?MM:"0"+MM[0]) 
		+ '-' + (dd[1]?dd:"0"+dd[0])
		+ ' ' + (hh[1]?hh:"0"+hh[0])
		+ ':' + (mm[1]?mm:"0"+mm[0])
		+ ':' + (ss[1]?ss:"0"+ss[0])
		; // padding
} ;
	
function run(argv) {
  
	var filePath = null ;
	var calendar = 'voice-recorder-pro' ;
	for( var index = 0 ; index < argv.length; index++){
		if ( argv[index] == '--file' ) {
			index++;
			filePath = argv[index] ;
		}
		if ( argv[index] == '--calendar' ) {
			index++;
			calendar = argv[index] ;
		}
	} 
	
	var validExtensions = [ '.m4a' ] ;
	var extension = filePath.substr(filePath.length - 4) ;
	
	terminalDoScript( 'logger -t "AS transcribe-audio-to-text" "' + filePath + '.started"') ;
	
	if ( validExtensions.indexOf(extension) > -1) {

		var textEditAlreadyOpen = true ;
		if (!applicationIsOpen('TextEdit')){
			/*
			 * https://github.com/dtinth/JXA-Cookbook/wiki/Getting-the-Application-Instance#checking-that-an-application-is-running-before-using-application
		   * */
			$.NSWorkspace.sharedWorkspace.launchApplication('/Applications/TextEdit.app') ;
			delay(1) ;
			textEditAlreadyOpen = false ;
		}
		
		var textEdit = Application('TextEdit') ;
		var quickTimePlayer = Application('QuickTime Player') ;
		var systemEvents = Application('System Events') ;
	  var textEditProcess = systemEvents.processes.byName('TextEdit') ;
	
		textEdit.includeStandardAdditions = true ;
		quickTimePlayer.includeStandardAdditions = true ;
	
		/*
		 * get quick time player ready
		 */
		 var document = Path(filePath) ;
		 quickTimePlayer.activate() ;
		 applicationCloseAll(quickTimePlayer) ;
		 delay(0.25);
		 quickTimePlayer.open(document) ;
		 document = quickTimePlayer.documents[0] ;
	 
		 textEdit.activate() ;
		 applicationCloseAll(textEdit) ;
	 
		 delay(0.25);
		 var textEditDocument = textEdit.make( {new: 'document'}) ;
		 delay(0.25);
	
		 textEdit.activate() ;
		 delay(0.25);
		 var startDictationDelay = 3 ;
		 stopDictation(systemEvents, startDictationDelay ) ;			
		 startDictation(systemEvents, startDictationDelay ) ;
			
		 document.play() ;
	 
		 var finishedPlaying = false ;
		 while(!finishedPlaying){
		 	delay(1);
			finishedPlaying = !document.playing() ;
		 }
	  
		 document.close() ;
		 quickTimePlayer.quit() ;

		 var textFilePath = filePath + '.transcribed.txt' ;
		 textEditMakePlainText( textEdit, textEditProcess, {afterClickDelay:2, verbose: true } ) ;
		 var text = textEditSaveAsText(textEdit,textEditDocument,{ verbose: true, afterSaveDelay: 0.5, filePath: textFilePath }) ;
		 console.log( "text: " + text ) ;
		 textEditDocument.close({saving: 'no'}) ;
		 
		 stopDictation(systemEvents, startDictationDelay ) ;

		 createEventInCalendar('voice-recorder-pro',textFilePath,text) ;
		 
		 if (!textEditAlreadyOpen) textEdit.quit() ;
		 
	}	 else {
		
		if ( false ) {
	
			terminalDoScript( 'logger -t "AS transcribe-audio-to-text" "' + filePath + ' : invalid extension"') ;
			/*
			 * for some reason the calendar event is not being created when run from automator
			 * let's see if we can call it from here
			 * */
			 if ( filePath.indexOf('m4a.transcribed.txt') > -1 ) {
				
					terminalDoScript( 'logger -t "AS transcribe-audio-to-text" "' + filePath + ' : attempting to add an event"') ;
					/*
					 * getting the current directory
					 * http://stackoverflow.com/questions/28773207/how-to-get-posix-path-of-the-current-scripts-folder-in-javascript-for-automatio
					 */
					//thePath = Application.currentApplication().pathTo(this) ;
					//thePathStr = $.NSString.alloc.init;
					//thePathStr = $.NSString.alloc.initWithUTF8String(thePath);
					//thePathStrDir = (thePathStr.stringByDeletingLastPathComponent);
					//commandLine = thePathStrDir.js + "/test-create-calendar-event.js --file " + filePath.split(' ').join('\\ ') ;
					commandLine = "test-create-calendar-event.js --file " + filePath.split(' ').join('\\ ') ;
				
					terminalDoScript( 'logger -t "AS transcribe-audio-to-text" "' +  commandLine + '"') ;
					terminalDoScript( commandLine ) ;
				}		
		  }
	 }
}

function applicationCloseAll(application){
	var documents = application.documents() ;
	while( documents.length > 0) {
		var document = documents[0] ;
		document.close({saving: 'no'}) ;
		var documents = application.documents() ;	
	}
}


function applicationIsOpen(applicationName){
	
	var result = false ;
	var systemEvents = Application('System Events') ;
	var processes = systemEvents.processes() ;
	for( var index=0; index < processes.length ; index++ ){
		var process = processes[index] ; 
		var title = process.title() ;
		if ( title === applicationName ){
			result = true ;
			break ;
		}
	}
	return result ;
}

function getAudioHijackReady(audioHijack,audioHijackProcess,systemEvents) {
	
	audioHijack.activate() ;
	delay(1) ;
	systemEvents.keystroke('1', { using: "command down"}) ;
	delay(1) ;

	var audioHijackSessions = audioHijackProcess.windows.byName("Audio Hijack 3").scrollAreas.at(0).uiElements.byName("Sessions") ;
	var targetName = 'quick-time-player-to-sound-flower-session' ;
	var uiElementsLength = audioHijackSessions.uiElements.length ;

	var sessionUiElement = null ; 

	for ( var index = 0; index < uiElementsLength; index++) {
		var staticTextName = audioHijackSessions.uiElements[index].staticTexts[0].name() ;
		if ( targetName == staticTextName){
			sessionUiElement = audioHijackSessions.uiElements[index] ;
		}
	}

	if ( sessionUiElement ) {
			sessionUiElement.click() ;
			delay(.25) ;
			var audioHijackWindow = audioHijackProcess.windows[targetName] ;
			var runCheckbox = audioHijackWindow.checkboxes['Run'] ;
			if ( runCheckbox.value() == 0 ) {
				runCheckbox.click() ;
			}
	}
	
}

function createEventInCalendar(calendar,textFilePath,text) {
		
	var application = Application.currentApplication() ;
	application.includeStandardAdditions = true ;

	var pathElements = textFilePath.split('/') ;
	var lastElement = pathElements[pathElements.length-1] ;
	console.log(lastElement) ;
	
	/*
	 * assumes the pattern yyyyMMdd-hhmmss at the start of the file name.
	 * */
	var yyyymmdd_hhmmss = lastElement.split('.')[0] ;
	console.log("yyyymmdd_hhmmss: " + yyyymmdd_hhmmss);
	var yyyymmdd = yyyymmdd_hhmmss.split('-')[0] ;
	var hhmmss = yyyymmdd_hhmmss.split('-')[1] ;

	var startDate = new Date(
			yyyymmdd.substr(0,4)
		, yyyymmdd.substr(4,2) - 1 
		, yyyymmdd.substr(6,2)
		, hhmmss.substr(0,2)
		, hhmmss.substr(2,2)
		, hhmmss.substr(4,2) 
	) ;
	console.log(startDate.toISOLocal()) ;
	
	var endDate = new Date(	yyyymmdd.substr(0,4)
		, yyyymmdd.substr(4,2) - 1 
		, yyyymmdd.substr(6,2)
		, hhmmss.substr(0,2)
		, hhmmss.substr(2,2)
		, hhmmss.substr(4,2) 
	) ;
	endDate.setSeconds( endDate.getSeconds() + 300 ) ;
	console.log(endDate.toISOLocal()) ;	

	/*
	 * '{"calendar":"voice-recorder-pro","title":"First event from the command line","startDate":"2016-01-05 10:56:00","endDate":"2016-01-05 11:16:00","notes":"notes"}'
	 */
	data = {}
	data.calendar = calendar ;
	data.title = lastElement ;
	data.startDate = startDate.toISOLocal() ;
	data.endDate = endDate.toISOLocal() ;
	data.notes = text + '\ncreated by createEventInCalendar';
	
	/*
	 * getting the current directory
	 * http://stackoverflow.com/questions/28773207/how-to-get-posix-path-of-the-current-scripts-folder-in-javascript-for-automatio
	 */
	//thePath = application.pathTo(this) ;
	//thePathStr = $.NSString.alloc.init;
	//thePathStr = $.NSString.alloc.initWithUTF8String(thePath);
	//thePathStrDir = (thePathStr.stringByDeletingLastPathComponent);
	
	//commandLine = thePathStrDir.js + "/create-event-in-calendar.swift '" + JSON.stringify(data).replace(/'/g, "’") + "'" ;
	commandLine = "create-event-in-calendar.swift '" + JSON.stringify(data).replace(/'/g, "’") + "'" ;
	terminalDoScript(commandLine) ;

}


function stopAudioHijack(audioHijack,audioHijackProcess,systemEvents){
	
	audioHijack.activate() ;
	delay(0.25) ;

	var targetName = 'quick-time-player-to-sound-flower-session' ;

	var audioHijackWindow = audioHijackProcess.windows[targetName] ;
	var runCheckbox = audioHijackWindow.checkboxes['Run'] ;
	if ( runCheckbox.value() == 1 ) {
		runCheckbox.click() ;
	}

}

/* 
 * https://github.com/dtinth/JXA-Cookbook/wiki/System-Events
 * */
function startDictation(systemEvents,delayAfterStart){
	
	var process = Application('System Events').processes.byName('TextEdit') ;
	var editMenuItems = process.menuBars[0].menuBarItems.byName('Edit').menus[0].menuItems() ;
	var dictationMenuItem = null ;
	var started = false ;
	
	for ( var index = 0 ; index < editMenuItems.length; index++) {
	  var editMenuItem = editMenuItems[index] ;
		var title = editMenuItem.title() ;
		var indexOfDictation = title.indexOf('Dictation') ; 
		if ( indexOfDictation > 0 ) {
			var dictationVerb = title.substr(0,indexOfDictation).trim() ;
			console.log(title);
			console.log(dictationVerb) ;
			editMenuItem.click()
			delay(delayAfterStart) ;
			if ( dictationVerb == 'Start' ) {
				started = true ;
			} else {
				// start after stop or cancel
				startDictation(systemEvents,delayAfterStart) ;
			}
		}
	}
	return started ;
	
}

function stopDictation(systemEvents,delayAfterStop){
	
	var process = Application('System Events').processes.byName('TextEdit') ;
	var editMenuItems = process.menuBars[0].menuBarItems.byName('Edit').menus[0].menuItems() ;
	var dictationMenuItem = null ;
	var stopped = false ;
	
	for ( var index = 0 ; index < editMenuItems.length; index++) {
	  var editMenuItem = editMenuItems[index] ;
		var title = editMenuItem.title() ;
		var indexOfDictation = title.indexOf('Dictation') ; 
		if ( indexOfDictation > 0 ) {
		
			var dictationVerb = title.substr(0,indexOfDictation).trim() ;
			console.log(title);
			console.log(dictationVerb) ;
			
			if ( dictationVerb == 'Start' ) {
				stopped = true ;
			} else {
				// start after stop or cancel
				editMenuItem.click() ;
				delay(delayAfterStop) ;		
				stopped = stopDictation(systemEvents,delayAfterStop) ;
			}
		}
	}
	return stopped ;
}

/* 
 * Having too many windows open in terminal causes the terminal.doScript() function call to fail
 * ; exit 0 closes the terminal window if you have Terminal set to close windows after a successful exit.
 * which is not the default ... you have to change the Terminal preferences to this if you expect this to work.
 * */
function terminalDoScript(commandLine){
	var terminal = Application('Terminal') ;
	terminal.doScript(commandLine + '; sleep 2; exit 0') ; 
}

function textEditMakePlainText(textEdit,textEditProcess,options){
	
	if (!options.verbose) options.verbose = false ;
  if (!options.delayAfterClick) options.delayAfterClick = 0.5 ;
	if (!options.afterTextEditActivateDelay) options.afterTextEditActivateDelay = 1 ;
	
	textEdit.activate() ;
	delay(options.afterTextEditActivateDelay) ;
	var formatMenuItems = textEditProcess.menuBars[0].menuBarItems.byName('Format').menus[0].menuItems() ;
	
	for ( var index = 0; index < formatMenuItems.length; index++){
	
		var formatMenuItem = formatMenuItems[index] ;
		var title = formatMenuItem.title() ;
		var richIndex = title.indexOf('Rich') ;
		var plainIndex = title.indexOf('Plain') ;
		if ( richIndex > -1 || plainIndex > -1) {
			if( options.verbose) console.log(formatMenuItem.title()) ;	
			if ( plainIndex > -1 ) {
				formatMenuItem.click() ;
				delay(options.delayAfterClick) ;
			}
		}
	}
}

function textEditSaveAsText(textEdit,textEditDocument,options){

	if (!options.verbose) options.verbose = false ;
	if (!options.afterSaveDelay) options.delayAfterClick = 1 ;
	
	var text = textEditDocument.text() + '\n' ;
	if( options.verbose ) console.log(text) ;
	var textNSString = $.NSString.alloc.initWithString(text) ;
	if( options.verbose ) console.log(text) ;

	textNSString.writeToFileAtomicallyEncodingError(options.filePath, true, $.NSUTF8StringEncoding, null);
	delay( options.afterSaveDelay ) ;

	return text ;
}