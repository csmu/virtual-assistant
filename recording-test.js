#!/usr/bin/env osascript -l JavaScript

function run(argv) {
	
	var systemEvents = Application('System Events') ;
	var textEdit = Application('TextEdit') ;
	var quickTimePlayer = Application('QuickTime Player') ;
	var audioDocument = quickTimePlayer.documents()[0] ;
	


	//textEdit.activate() ;
	delay(1) ;

	// toggleDictation(systemEvents) ;
	
	quickTimePlayer.play( audioDocument ) ;		
	
}

/* 
 * https://github.com/dtinth/JXA-Cookbook/wiki/System-Events
 * */
function toggleDictation(systemEvents){
	
	var processes = systemEvents.processes ;
	
	for ( var index = 0 ; index < processes.length; index++){
		var process = processes[index];
		if( process.frontmost) {
			console.log('front process found') ;
			break ;
		}
	}
	
	var editMenuItem = process.menuBars[0].menuBarItems.byName('Edit') ;
	var startDictationMenuItem = editMenuItem.menus[0].menuItems.byName('Start Dictation') ;
	var stopDictationMenuItem = editMenuItem.menus[0].menuItems.byName('Stop Dictation') ;
	var cancelDictationMenuItem = editMenuItem.menus[0].menuItems.byName('Cancel Dictation') ;
	
	try {
		console.log( 'stopDictationMenuItem.click()' ) ;
		stopDictationMenuItem.click() ;
	} catch( error ) {
		console.log( error.message ) ;
	}
	
	delay(1) ;
	
	try {
		console.log( 'startDictationMenuItem.click()' ) ;
		startDictationMenuItem.click() ;
	} catch( error ) {
		console.log( error.message ) ;
	}

	delay(1) ;
		
}