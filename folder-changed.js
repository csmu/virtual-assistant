#!/usr/bin/env node

console.log("FolderChanged:") ;

var childProcess = require('child_process'), convertFilesToWav ;
var path = require('path') ;
var verbose = false ;


var scriptPath = path.join( __dirname, "convert-sound-files-in-folder.js" ) ;
var commandLine = scriptPath + ' --directory \'/Users/admin/Dropbox/Apps/Voice Record Pro\'' + ' --to wav' ;
if ( verbose || true ) console.log( commandLine ) ;


/*
	https://docs.nodejitsu.com/articles/child-processes/how-to-spawn-a-child-process
	*/
convertFilesToWav = childProcess.exec( commandLine, function (error, stdout, stderr) {
   if (error) {
		 console.log( JSON.stringify( error.stack ) ) ;
     console.log('Error code: '+error.code);
     console.log('Signal received: '+error.signal);
   }
   console.log('Child Process STDOUT: ' + stdout);
   console.log('Child Process STDERR: ' + stderr);
}); 
 
convertFilesToWav.on('exit', function (code) {
  console.log('Files have been converted to wav '+code);
});
	