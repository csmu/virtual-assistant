#!/usr/bin/env osascript -l JavaScript
ObjC.import('Foundation');

/*
 * http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
 * */
Date.prototype.toISOLocal = function() {
   var yyyy = this.getFullYear().toString();
   var MM = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
	 var hh = this.getHours().toString() ;
	 var mm = this.getMinutes().toString() ;
	 var ss = this.getSeconds().toString() ;
   return yyyy 
	 	+ '-' + (MM[1]?MM:"0"+MM[0]) 
		+ '-' + (dd[1]?dd:"0"+dd[0])
		+ ' ' + (hh[1]?hh:"0"+hh[0])
		+ ':' + (mm[1]?mm:"0"+mm[0])
		+ ':' + (ss[1]?ss:"0"+ss[0])
		; // padding
  };
	
function run(argv) {
  
	var filePath = null ;
	for( var index = 0 ; index < argv.length; index++){
		if ( argv[index] == '--file' ) {
			index++;
			filePath = argv[index] ;
		}
	}
	
	var application = Application.currentApplication() ;
	application.includeStandardAdditions = true ;
	
	if ( filePath ) {
		
		var commandLine = 'ls -l ' + filePath.split(' ').join('\\ ') ;
		console.log( commandLine ) ;
		console.log( application.doShellScript( commandLine )) ;
		
		commandLine = 'cat ' + filePath.split(' ').join('\\ ') + " | tr '\r' '\n' ";
		var contents = application.doShellScript( commandLine ) ;
		console.log(contents) ;
		
		var pathElements = filePath.split('/') ;
		var lastElement = pathElements[pathElements.length-1] ;
		console.log(lastElement) ;
		
		/*
		 * assumes the pattern yyyyMMdd-hhmmss at the start of the file name.
		 * */
		var yyyymmdd_hhmmss = lastElement.split('.')[0] ;
		console.log("yyyymmdd_hhmmss: " + yyyymmdd_hhmmss);
		var yyyymmdd = yyyymmdd_hhmmss.split('-')[0] ;
		var hhmmss = yyyymmdd_hhmmss.split('-')[1] ;
		var startDateText = yyyymmdd.substr(0,4)
			+ '-' + yyyymmdd.substr(4,2)
			+ '-' + yyyymmdd.substr(6,2)
			+ ' ' + hhmmss.substr(0,2)
			+ ':' + hhmmss.substr(2,2)
			+ ':' + hhmmss.substr(4,2) 
		 ;  
		console.log(startDateText) ;
		var startDate = new Date(
				yyyymmdd.substr(0,4)
			, yyyymmdd.substr(4,2) - 1 
			, yyyymmdd.substr(6,2)
			, hhmmss.substr(0,2)
			, hhmmss.substr(2,2)
			, hhmmss.substr(4,2) 
		) ;
		console.log(startDate.toISOLocal()) ;
		var endDate = new Date(	yyyymmdd.substr(0,4)
			, yyyymmdd.substr(4,2) - 1 
			, yyyymmdd.substr(6,2)
			, hhmmss.substr(0,2)
			, hhmmss.substr(2,2)
			, hhmmss.substr(4,2) 
		) ;
		endDate.setSeconds( endDate.getSeconds() + 300 ) ;
		var endDateText = endDate.toISOLocal(); 
		console.log(endDateText) ;
		
		/*
		 * '{"calendar":"voice-recorder-pro","title":"First event from the command line","startDate":"2016-01-05 10:56:00","endDate":"2016-01-05 11:16:00","notes":"notes"}'
		 */
		data = {}
		data.calendar = "voice-recorder-pro" ;
		data.title = lastElement ;
		data.startDate = startDateText ;
		data.endDate = endDateText ;
		data.notes = contents + '\ncreated by test-create-calendar-event' ;
		commandLine = "create-event-in-calendar.swift '" +  JSON.stringify(data).replace('\\','') + "'" ;
		console.log(commandLine) ;
		application.doShellScript( commandLine ) ;
	} 
}