Virtual assistant is designed to process audio messages and then determine if anything can be done.
It's very alpha at the moment but I use it myself to capture a record of audio notes on my calendars.
The great feature of virtual assistant is it uses Apple's dictation feature to do a reasonable job of transcribing audio to text.

The basic workflow design I use at 2016-01-07 is

	1. Record an m4a audio file on Voice Record Pro. http://www.bejbej.info/app/voicerecordpro
	2. Upload the file to drop box
	3. I have a automator script watching the Voice Record Pro drop box folder on my Mac that fires up
	./transcribe-audio-to-text.js --file ~/Dropbox/Apps/Voice\ Record\ Pro/20160105-070937.m4a --calendar voice-recorder-pro
	which transcribes the audio and places a record on the calendar.

Dependencies.

	SoundFlower 2.0b2 is used to take output from quicktime to send to dictation.
	The system sound output needs to be set to soundflower 2 ch and dication needs to be set to listen to the soundflower 2 ch.
	
	SoundFlower 2.0b2 https://github.com/mattingalls/Soundflower/releases
	fork-backup https://github.com/csmu-cenr/Soundflower
	
	tccutil.py is used to allow osascript to control your Mac.
	
	tccutil.py	https://github.com/jacobsalmela/tccutil
	fork-backup	https://github.com/csmu-cenr/tccutil
	
	Automator to Watch a folder for new files.
	
	the core step is run script
	
	for path in "$@"
	do
	  /source/virtual-assistant/transcribe-audio-to-text.js --file "$path"
	done
	
	The workflow I used has been included in the repository.
	

Example call to transribe an audio file and place it a calendar.

    ./transcribe-audio-to-text.js --file ~/Dropbox/Apps/Voice\ Record\ Pro/20160105-070937.m4a --calendar voice-recorder-pro

Example call to create an event from the command line.
The swift script assumes that you have read write access to the calendar and that the calendar exists.
Note passing the arguments as a json string.

    ./create-event-in-calendar.swift '{"calendar":"voice-recorder-pro","title":"First event from the command line","startDate":"2016-01-05 10:56:00","endDate":"2016-01-05 11:16:00","notes":"notes"}'
