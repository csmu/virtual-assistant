#!/usr/bin/env xcrun swift 
 
import EventKit ;

// Create an Event Store instance to access the calendars and to create an event
let eventStore = EKEventStore() ;

func addEventToCalendar(data:AnyObject,calendar:EKCalendar,store:EKEventStore) -> Bool {
	
	var result : Bool = false ;
	let dateFormatter = NSDateFormatter() ;
	dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss" ;
	
	let title:String = (data["title"] as AnyObject? as? String) ?? "" ;
	let startDateText:String = (data["startDate"] as AnyObject? as? String) ?? "";	
	let endDateText:String = (data["endDate"] as AnyObject? as? String) ?? "";		
	let notes:String = (data["notes"] as AnyObject? as? String) ?? "";		

	let startDate:NSDate! = dateFormatter.dateFromString(startDateText) ;
	let endDate:NSDate! = dateFormatter.dateFromString(endDateText) ;


  let event = EKEvent(eventStore: store)
	
  event.title = title ;
  event.startDate = startDate ;
  event.endDate = endDate ;
	//event.location = (data["location"] as AnyObject? as? String) ?? "" ;
  event.notes = notes ;
  event.calendar = calendar ;
	
	let thisEvent:EKSpan = EKSpan.ThisEvent ;
	
	do {
		try store.saveEvent(event, span: thisEvent, commit: true);
		result = true ;
		
	} catch let error as NSError {
	
  	print("store.saveEvent error: \(error.localizedDescription)")
	 
	}
			 
	return result ;
}

func getCalendarsByName(calendarName: String) -> Array<EKCalendar> {
    var result: Array<EKCalendar> = [] ;
		let calendars = eventStore.calendarsForEntityType(EKEntityType.Event)
 	         as [EKCalendar] ;
		 for calendar in calendars as [EKCalendar] {
	     if ( calendarName == calendar.title ) {
				 result.append(calendar);
	     }
		 }
    return result ;
}

/*
 * get the json string arguument
 * assumes only one argument
 * */
var argumentJSONString = String() ;
for argument in Process.arguments {
  argumentJSONString = argument ;
}


 
/*
 * convert the string to data
 * assumes valkid input create by machine code
 * */
var argumentData: NSData = argumentJSONString.dataUsingEncoding(NSUTF8StringEncoding)! ;
var error: NSError? ;

do {

	 var targeCalendarFound = false ;
   let argumentObject:AnyObject = try NSJSONSerialization.JSONObjectWithData(argumentData, options: []) ;
	 let targetCalendarName = (argumentObject["calendar"] as AnyObject? as? String) ?? "";	 
	 
	 /*
	  * find the calendar
	  * */
	 let targetCalendarArray:Array<EKCalendar> = getCalendarsByName(targetCalendarName) 
	 if ( targetCalendarArray.count > 0 ){
		 let calendar = targetCalendarArray[0] ;
		 if ( addEventToCalendar(argumentObject, calendar: calendar, store: eventStore ) ) {
		 	
		 } else {
		 	 print ("No event was added") ; 
		 }
	 }
	 
} catch let error as NSError {
	
   print("json error: \(error.localizedDescription)")
	 
}
			 

/*
 * http://stackoverflow.com/questions/28993784/adding-events-and-calendars-with-eventkit
 * */


