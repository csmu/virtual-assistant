#!/usr/bin/env osascript -l JavaScript

function run(argv) {
  
	var applicationName = null ;
	for( var index = 0 ; index < argv.length; index++){
		if ( argv[index] == '--application-name' ) {
			index++;
			applicationName = argv[index] ;
		}
	} 
	
	
	var applicationOpen = false ;
	applicationOpen = applicationIsOpen(applicationName) ;
	console.log( applicationName + '.isOpen: ' + applicationOpen ) ;
	
}

function applicationIsOpen(applicationName){
	
	var result = false ;
	var systemEvents = Application('System Events') ;
	var processes = systemEvents.processes() ;
	for( var index=0; index < processes.length ; index++ ){
		var process = processes[index] ; 
		var title = process.title() ;
		if ( title === applicationName ){
			result = true ;
			break ;
		}
	}
	return result ;
}