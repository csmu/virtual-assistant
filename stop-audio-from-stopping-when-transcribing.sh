
# http://telestreamblog.telestream.net/2013/12/using-dictation-to-turn-recorded-audio-to-text-2/
defaults write com.apple.SpeechRecognitionCore AllowAudioDucking -bool NO
defaults write com.apple.speech.recognition.AppleSpeechRecognition.prefs DictationIMAllowAudioDucking -bool NO

# To restore your system to it’s virginal state, run these commands in Terminal and then restart dictation:

#defaults delete com.apple.SpeechRecognitionCore AllowAudioDucking
#defaults delete com.apple.speech.recognition.AppleSpeechRecognition.prefs DictationIMAllowAudioDucking


